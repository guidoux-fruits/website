import * as argon2 from 'argon2';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

async function main() {
  const admin = await prisma.user.upsert({
    where: { username: 'admin' },
    update: {},
    create: {
      username: 'admin',
      email: process.env.WEBSITE_ADMIN_EMAIL as string,
      password: await argon2.hash(process.env.WEBSITE_ADMIN_PASSWORD as string),
      enabled: true,
    },
  });

  const content = await prisma.content.upsert({
    where: { id: '1' },
    update: {},
    create: {
      message: 'Le site est en construction',
      description: 'This is the first content of the website.',
      homepage: 'Bienvenue chez Guidoux Fruits',
    },
  });

  console.log({ admin, content });
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
