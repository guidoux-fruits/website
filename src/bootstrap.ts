import * as nunjucks from 'nunjucks';
import * as cookieParser from 'cookie-parser';
import { NestExpressApplication } from '@nestjs/platform-express';
import { join } from 'path';

export async function bootstrap(
  app: NestExpressApplication,
): Promise<NestExpressApplication> {
  nunjucks.configure(join(__dirname, '..', 'views'), {
    express: app,
    watch: process.env.NODE_ENV === 'development',
  });

  app.setBaseViewsDir(join(__dirname, '..', 'views'));
  app.useStaticAssets(join(__dirname, '..', 'public'));
  app.setViewEngine('njk');
  app.use(cookieParser());

  return app;
}
