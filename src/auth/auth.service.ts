import * as argon2 from 'argon2';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User } from '@prisma/client';
import { UsersService } from 'src/users/users.service';
import { LoginUser } from 'src/auth/types/login-user.type';
import { JwtPayload } from 'src/auth/types/jwt-payload';
import { Jwt } from 'src/auth/types/jwt';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateCredentials({ username, password }: LoginUser): Promise<User> {
    const user = (await this.usersService.getUserByUsername(username)) as User;

    const passwordsMatch = await argon2.verify(user.password, password);

    if (!user.enabled || !passwordsMatch) {
      throw new UnauthorizedException();
    }

    return user;
  }

  async generateJwt(user: User): Promise<Jwt> {
    const payload: JwtPayload = {
      sub: user.id,
      username: user.username,
      email: user.email,
    };

    return {
      jwt: await this.jwtService.signAsync(payload),
    };
  }

  async validateJwtPayload({ sub }: JwtPayload): Promise<User> {
    const user = (await this.usersService.getUser(sub)) as User;

    if (!user.enabled) {
      throw new UnauthorizedException();
    }

    return user;
  }
}
