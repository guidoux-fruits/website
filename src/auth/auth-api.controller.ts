import { Controller, Post, Res, Body, HttpCode } from '@nestjs/common';
import { Response } from 'express';
import { ApiTags } from '@nestjs/swagger';
import { User } from '@prisma/client';
import { AuthService } from 'src/auth/auth.service';
import { SignupUserDto } from 'src/auth/dtos/signup-user.dto';
import { UsersService } from 'src/users/users.service';
import { LocalAuth } from 'src/auth/local/local-auth.decorator';
import { JwtDto } from 'src/auth/dtos/jwt.dto';
import { JwtAuth } from 'src/auth/jwt/jwt-auth.decorator';
import { AuthUser } from 'src/auth/decorators/auth-user.decorator';
import { JWT_AUTH_KEY } from 'src/auth/jwt/jwt.strategy';

@ApiTags('Auth')
@Controller('api/auth')
export class AuthApiController {
  constructor(
    private authService: AuthService,
    private usersService: UsersService,
  ) {}

  @Post('login')
  @LocalAuth()
  @HttpCode(200)
  async login(@AuthUser() user: User, @Res() res: Response) {
    const jwt = await this.authService.generateJwt(user);

    res.cookie(JWT_AUTH_KEY, jwt.jwt, {
      httpOnly: true,
      sameSite: true,
      secure: true,
    });

    // Return json web token
    return res.json(new JwtDto(jwt));
  }

  @Post('logout')
  @JwtAuth()
  @HttpCode(204)
  async logout(@Res() res: Response) {
    res.cookie(JWT_AUTH_KEY, '', {
      httpOnly: true,
      sameSite: true,
      secure: true,
    });

    res.end();
  }

  @Post('signup')
  @HttpCode(200)
  async signup(@Body() signupUserDto: SignupUserDto) {
    await this.usersService.createUser(signupUserDto);
  }
}
