import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { JWT_AUTH_KEY } from 'src/auth/jwt/jwt.strategy';

@Injectable()
export class JwtAuthGuard extends AuthGuard(JWT_AUTH_KEY) {}
