import { Controller, Get, Render } from '@nestjs/common';

@Controller('auth')
export class AuthViewsController {
  @Get('login')
  @Render('auth/login')
  loginView() {
    return;
  }

  @Get('signup')
  @Render('auth/signup')
  signupView() {
    return;
  }
}
