import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JWT_EXPIRATION_TIME, JWT_SECRET } from 'src/config/config.constants';
import { UsersModule } from 'src/users/users.module';
import { AuthService } from 'src/auth/auth.service';
import { JwtStrategy } from 'src/auth/jwt/jwt.strategy';
import { LocalStrategy } from 'src/auth/local/local.strategy';
import { AuthApiController } from 'src/auth/auth-api.controller';
import { AuthViewsController } from 'src/auth/auth-views.controller';

@Module({
  imports: [
    ConfigModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get(JWT_SECRET, { infer: true }),
        signOptions: {
          expiresIn: configService.get(JWT_EXPIRATION_TIME, { infer: true }),
        },
      }),
    }),
    PassportModule,
    UsersModule,
  ],
  providers: [AuthService, JwtStrategy, LocalStrategy],
  controllers: [AuthApiController, AuthViewsController],
  exports: [AuthService],
})
export class AuthModule {}
