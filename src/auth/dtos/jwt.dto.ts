import { Jwt } from 'src/auth/types/jwt';

export class JwtDto implements Jwt {
  // The JWT to access protected resources
  jwt: string;

  constructor(partial: Partial<JwtDto>) {
    Object.assign(this, partial);
  }
}
