import { PickType } from '@nestjs/swagger';
import { LoginUser } from 'src/auth/types/login-user.type';
import { UserDto } from 'src/users/dtos/user.dto';

export class LoginUserDto
  extends PickType(UserDto, ['username', 'password'] as const)
  implements LoginUser {}
