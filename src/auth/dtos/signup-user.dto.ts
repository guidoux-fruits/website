import { PickType } from '@nestjs/swagger';
import { SignupUser } from 'src/auth/types/signup-user.type';
import { UserDto } from 'src/users/dtos/user.dto';

export class SignupUserDto
  extends PickType(UserDto, ['username', 'email', 'password'] as const)
  implements SignupUser {}
