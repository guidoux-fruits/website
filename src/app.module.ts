import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AuthModule } from 'src/auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import { PrismaModule } from 'nestjs-prisma';
import { UsersModule } from 'src/users/users.module';
import { ContentsModule } from '@/contents/contents.module';
import { ContentsService } from '@/contents/contents.service';
@Module({
  imports: [
    AuthModule,
    ConfigModule,
    PrismaModule.forRoot(),
    UsersModule,
    ContentsModule,
  ],
  controllers: [AppController],
  providers: [ContentsService],
})
export class AppModule {}
