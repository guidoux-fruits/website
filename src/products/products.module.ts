import { ContentsService } from '@/contents/contents.service';
import { ProductsApiController } from '@/products/products-api.controller';
import { ProductsViewsController } from '@/products/products-views.controller';
import { ProductsService } from '@/products/products.service';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { PrismaModule } from 'nestjs-prisma';

@Module({
  imports: [ConfigModule, PrismaModule],
  providers: [ProductsService],
  controllers: [ProductsApiController, ProductsViewsController],
  exports: [ProductsService],
})
export class ProdutctsModule {}
