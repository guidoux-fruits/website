import { Controller } from '@nestjs/common';
import { ProductsService } from '@/products/products.service';

@Controller('api/products')
export class ProductsApiController {
  constructor(private readonly productsService: ProductsService) {}
}
