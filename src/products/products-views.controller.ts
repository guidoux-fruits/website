import { Controller } from '@nestjs/common';

import { ConfigService } from '@nestjs/config';
import { ProductsService } from '@/products/products.service';

@Controller('products')
export class ProductsViewsController {
  constructor(
    private readonly productsService: ProductsService,
    readonly configService: ConfigService,
  ) {}
}
