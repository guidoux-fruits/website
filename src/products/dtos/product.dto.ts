import { Product } from '@prisma/client';

export class ProductDto implements Product {
  title: string;
  description: string | null;
  price: number;
  quantity: string;
  price2: number;
  quantity2: string;
  comment: string;
  comment2: string;
  from: boolean;
  id: string;
}
