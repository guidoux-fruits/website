import { ProductDto } from '@/products/dtos/product.dto';

export class ReadProductDto extends ProductDto {
  constructor(partial: Partial<ProductDto>) {
    super();

    Object.assign(this, partial);
  }
}
