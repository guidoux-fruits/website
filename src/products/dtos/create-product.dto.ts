import { ProductDto } from '@/products/dtos/product.dto';
import { OmitType } from '@nestjs/swagger';

export class CreateProductDto extends OmitType(ProductDto, ['id'] as const) {}
