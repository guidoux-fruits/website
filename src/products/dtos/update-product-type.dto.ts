import { ProductDto } from '@/products/dtos/product.dto';
import { OmitType, PartialType } from '@nestjs/swagger';

export class UpdateProductDto extends PartialType(
  OmitType(ProductDto, ['id'] as const),
) {}
