import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { PrismaService } from 'nestjs-prisma';

@Injectable()
export class ProductsService {
  constructor(private readonly prisma: PrismaService) {}

  async getProducts() {
    return await this.prisma.product.findMany();
  }

  async getProduct(productId: string) {
    const product = await this.prisma.product.findFirstOrThrow({
      where: {
        id: {
          equals: productId,
        },
      },
    });

    return product;
  }

  async createProduct(createProduct: Prisma.ProductCreateInput) {
    const newProduct = await this.prisma.product.create({
      data: {
        ...createProduct,
      },
    });

    return newProduct;
  }

  async updateProduct(
    productId: string,
    updateProduct: Prisma.ProductUpdateInput,
  ) {
    const updatedProduct = await this.prisma.product.update({
      where: {
        id: productId,
      },
      data: {
        ...updateProduct,
      },
    });

    return updatedProduct;
  }

  async deleteProduct(productId: string) {
    await this.prisma.product.delete({
      where: {
        id: productId,
      },
    });
  }
}
