import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { PrismaService } from 'nestjs-prisma';

@Injectable()
export class ContentsService {
  constructor(private readonly prisma: PrismaService) {}

  async getContents() {
    return await this.prisma.content.findMany({});
  }

  getContent(id: string) {
    return this.prisma.content.findUnique({
      where: {
        id,
      },
    });
  }

  async updateContents(updateContent: Prisma.ContentUpdateInput) {
    const [contents] = await this.getContents();

    const contentId = contents.id;

    return await this.prisma.content.update({
      where: {
        id: contentId,
      },
      data: updateContent,
    });
  }
}
