import { ContentDto } from '@/contents/dtos/content.dto';
import { OmitType, PartialType } from '@nestjs/swagger';

export class UpdateContentDto extends PartialType(
  OmitType(ContentDto, ['id']),
) {}
