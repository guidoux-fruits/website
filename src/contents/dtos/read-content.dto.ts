import { ContentDto } from '@/contents/dtos/content.dto';
import { OmitType } from '@nestjs/swagger';
import { UserDto } from 'src/users/dtos/user.dto';

export class ReadContentDto extends ContentDto {
  constructor(partial: Partial<UserDto>) {
    super();

    Object.assign(this, partial);
  }
}
