import { Content } from '@prisma/client';

export class ContentDto implements Content {
  id: string;
  homepage: string;
  message: string;
  description: string;
}
