import { JwtAuth } from '@/auth/jwt/jwt-auth.decorator';
import { ContentsService } from '@/contents/contents.service';
import { ReadContentDto } from '@/contents/dtos/read-content.dto';
import { UpdateContentDto } from '@/contents/dtos/update-content.dto';
import { Body, Controller, Param, Patch, Post } from '@nestjs/common';

@Controller('api/contents')
export class ContentsApiController {
  constructor(private readonly contentsService: ContentsService) {}

  @Patch(':id')
  @JwtAuth()
  async createContentApi(
    @Param('id') id: string,
    @Body() updateContent: UpdateContentDto,
  ) {
    const newContent = await this.contentsService.updateContents(updateContent);
    return new ReadContentDto(newContent);
  }
}
