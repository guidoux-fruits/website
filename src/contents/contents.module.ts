import { ContentsApiController } from '@/contents/contents-api.controller';
import { ContentsViewsController } from '@/contents/contents-views.controller';
import { ContentsService } from '@/contents/contents.service';
import { Module } from '@nestjs/common';
import { PrismaModule } from 'nestjs-prisma';

@Module({
  imports: [PrismaModule],
  controllers: [ContentsApiController, ContentsViewsController],
  providers: [ContentsService],
  exports: [ContentsService],
})
export class ContentsModule {}
