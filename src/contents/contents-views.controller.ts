import { Get, Controller, Render, Param } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { User } from '@prisma/client';
import { JwtAuth } from '@/auth/jwt/jwt-auth.decorator';
import { AuthUser } from '@/auth/decorators/auth-user.decorator';
import { ContentsService } from '@/contents/contents.service';
import { ReadContentDto } from '@/contents/dtos/read-content.dto';
@ApiTags('Views')
@Controller('contents')
export class ContentsViewsController {
  constructor(private readonly contentsService: ContentsService) {}

  @Get()
  @JwtAuth()
  @Render('contents/index')
  async getContentsView(@AuthUser() user: User) {
    const contents = await this.contentsService.getContents();

    const contentsDto = contents.map((content) => new ReadContentDto(content));
    return {
      username: user?.username,
      email: user?.email,
      contents: contentsDto,
    };
  }

  @Get(':id/edit')
  @Render('contents/form')
  @JwtAuth()
  async editContentView(@AuthUser() user: User, @Param('id') id: string) {
    const content = await this.contentsService.getContent(id);
    let contentDto;
    if (content) {
      contentDto = new ReadContentDto(content);
    }

    return {
      username: user?.username,
      email: user?.email,
      id,
      values: contentDto,
      action: 'PATCH',
    };
  }
}
