import { NODE_ENV } from 'src/config/config.constants';

export const ConfigConfiguration = () => ({
  [NODE_ENV]: process.env.NODE_ENV as string,
});
