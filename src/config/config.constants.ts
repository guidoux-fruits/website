export const FQDN = 'WEBSITE_FQDN';
export const JWT_SECRET = 'WEBSITE_JWT_SECRET';
export const JWT_EXPIRATION_TIME = 'WEBSITE_JWT_EXPIRATION_TIME';
export const NODE_ENV = 'NODE_ENV';

export interface EnvironmentVariables {
  [FQDN]: string;
  [JWT_SECRET]: string;
  [JWT_EXPIRATION_TIME]: string;
  [NODE_ENV]: string;
}
