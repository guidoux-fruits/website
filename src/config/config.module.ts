import * as Joi from 'joi';
import { Module } from '@nestjs/common';
import { ConfigModule as NestConfigModule } from '@nestjs/config';
import { NODE_ENV } from 'src/config/config.constants';
import { ConfigConfiguration } from 'src/config/config.configuration';

@Module({
  imports: [
    NestConfigModule.forRoot({
      load: [ConfigConfiguration],
      validationSchema: Joi.object({
        [NODE_ENV]: Joi.string()
          .valid('development', 'test', 'production')
          .default('production'),
      }),
      validationOptions: {
        allowUnknown: true,
        abortEarly: false,
      },
    }),
  ],
  exports: [NestConfigModule],
})
export class ConfigModule {}
