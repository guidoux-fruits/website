import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AppModule } from 'src/app.module';
import { bootstrap } from 'src/bootstrap';

const main = async () => {
  const instance = await NestFactory.create<NestExpressApplication>(AppModule);

  const app = await bootstrap(instance);

  await app.listen(3000);
};

main();
