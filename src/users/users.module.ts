import { Module } from '@nestjs/common';
import { PrismaModule } from 'nestjs-prisma';
import { UsersApiController } from 'src/users/users-api.controller';
import { UsersService } from 'src/users/users.service';

@Module({
  imports: [PrismaModule],
  controllers: [UsersApiController, UsersApiController],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
