import { User } from '@prisma/client';

export class UserDto implements User {
  // Identification of the user
  id: string;

  // Username of the user
  username: string;

  // Email of the user
  email: string;

  // Password of the user
  password: string;

  // Set if the user is enabled (and have access to the platform)
  enabled = false;

  // Date when the user was created
  createdAt: Date;

  // Date when the user was updated
  updatedAt: Date;

  // Date when the user was deleted
  deletedAt: Date | null;
}
