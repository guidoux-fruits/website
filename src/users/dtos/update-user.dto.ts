import { OmitType, PartialType } from '@nestjs/swagger';
import { UserDto } from 'src/users/dtos/user.dto';

export class UpdateUserDto extends PartialType(
  OmitType(UserDto, ['id', 'createdAt', 'updatedAt', 'deletedAt'] as const),
) {}
