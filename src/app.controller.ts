import { AuthUser } from '@/auth/decorators/auth-user.decorator';
import { JwtAuth } from '@/auth/jwt/jwt-auth.decorator';
import { ContentsService } from '@/contents/contents.service';
import { Get, Controller, Render } from '@nestjs/common';
import { User } from '@prisma/client';

@Controller()
export class AppController {
  constructor(private readonly contentsService: ContentsService) {}
  @Get()
  @Render('index')
  async root() {
    const [content] = await this.contentsService.getContents();
    return { content };
  }

  @Get('home')
  @JwtAuth()
  @Render('home')
  home(@AuthUser() user: User) {
    return {
      username: user?.username,
      email: user?.email,
    };
  }
}
