# Guidoux Fruits website

## Prerequisites

The following prerequisites must be filled to run this service:

[Docker](https://docs.docker.com/get-docker/) must be installed.

[Visual Studio Code](https://code.visualstudio.com/download) must be installed

## Installation

Open this folder in Visual Studio Code, and open it in a dev contianer

```bash
npm install

cp .env.defaults .env

# change the .env file to your needs, especially the database password, and the JWT secret, the DATABASE_SERVICE and the WEBSITE_BACKEND_URL.

docker-compose up -d database
```

## Running the app

```bash
# development
npm run start

# watch mode
npm run start:dev

# production mode
npm run start:prod
```

## Access the documentation

The API documentation is on <http://localhost:3000/api>.

## Test

```bash
# unit tests
npm run test

# e2e tests
npm run test:e2e

# test coverage
npm run test:cov
```
